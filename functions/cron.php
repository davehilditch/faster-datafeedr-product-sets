<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * The interval (in seconds) in which the system 
 * should check for product sets which need to be updated.
 * 
 * By default: 1 minute (60 seconds)
 */
$awdcurTime = microtime(true);
function awdechotimings($awd_e) {
	global $awdcurTime;
	trigger_error(round(microtime(true) - $awdcurTime,3)*1000 . "ms - " . $awd_e, E_USER_NOTICE);
	echo "<p>" . round(microtime(true) - $awdcurTime,3)*1000 . "ms  - " . $awd_e . "</p>";
	$awdcurTime = microtime(true);
}
 
function dfrps_default_cron_interval() {
	$configuration = get_option( 'dfrps_configuration', array() );
/*	if ( !empty( $configuration ) ) {
		return $configuration['cron_interval'];
	} else {
		return 60;
	}
	*/
	return 1;
}

/**
 * Add a new interval for DFRPS to the cron schedules.
 */
add_filter( 'cron_schedules', 'dfrps_cron_schedules' );
function dfrps_cron_schedules( $array ) {
    $array['dfrps_schedule'] = array(
		'interval' 	=> dfrps_default_cron_interval(),
		'display' 	=> __( 'DFRPS Cron Schedule', DFRPS_DOMAIN ),
    );
    return $array;
}

/**
 * Schedule DFRPS cron event, but just once.
 */
if ( !wp_next_scheduled( 'dfrps_cron' ) ) {
    wp_schedule_event( time(), 'dfrps_schedule', 'dfrps_cron' );    	
}

/**
 * Query product sets and run update if needed.
 */
add_action( 'dfrps_cron', 'runProductSetsMultipleTimes' );

function runProductSetsMultipleTimes() {
	echo "<style>p {margin:0px;padding:0px;}</style>";

	awdechotimings( "JOB LOG - checking if I can start ");
	ini_set('max_execution_time', 3600);
	//todo: Need to fix this with a transaction - check and set a value with a read lock in the database at the row level
	$use_cache = wp_using_ext_object_cache( false );
	$doing_update = get_transient( 'dfrps_doing_update_awd5');
	wp_using_ext_object_cache( $use_cache );
	
	if ($doing_update != false && (round(microtime(true)) - $doing_update) < 3600 ) {
		awdechotimings( "JOB LOG - NOT STARTING: A job already started " . strval(round(microtime(true)) - $doing_update) . "s ago ");
		return false;
	} else {
		wp_suspend_cache_addition( true );
		wp_suspend_cache_invalidation( true );

		$use_cache = wp_using_ext_object_cache( false );
		set_transient( 'dfrps_doing_update_awd5', round(microtime(true)), (60 * 60) ); // this whole transient thing is in case the product sets crash i think, or in case another page calls the cron
		wp_using_ext_object_cache( $use_cache );
		
		if (!$doing_update) {
			awdechotimings( "JOB LOG - STARTING: No jobs running - no transient set");			
		}
		else {
			awdechotimings( "JOB LOG - STARTING: No jobs running - transient set but job stuck (" . strval(round(microtime(true)) - $doing_update) . " seconds ago the last job started)");			
		}
	}
	
	for ($i=0; $i<50; $i++) {
		awdechotimings( "JOB LOG - CRON LOOP $i");
		try {
			if (!dfrps_get_product_set_to_update()) {
				break; // product set still running from previous cron - break, let the other loop
			}
			
		}
		catch(Exception $e) {
			$use_cache = wp_using_ext_object_cache( false );
			delete_transient( 'dfrps_doing_update_awd5');
			wp_using_ext_object_cache( $use_cache );

			wp_suspend_cache_addition( false );
			wp_suspend_cache_invalidation( false );
			awdechotimings( "JOB LOG ERROR - JOB ENDED ABRUPTLY AFTER $i LOOPS");			
			awdechotimings( "JOB LOG ERROR DETAILS - " . print_r($e->getMessage(), true));			
			
		}
	}	
	$use_cache = wp_using_ext_object_cache( false );
	delete_transient( 'dfrps_doing_update_awd5');
	wp_using_ext_object_cache( $use_cache );

	wp_suspend_cache_addition( false );
	wp_suspend_cache_invalidation( false );
	awdechotimings( "JOB LOG - JOB ENDED AFTER $i LOOPS");			
}

function dfrps_get_product_set_to_update() {
	global $wpdb;

// Add indexes to optimise performance
	$indexexists = $wpdb->get_var("
	SELECT INDEX_NAME FROM INFORMATION_SCHEMA.STATISTICS WHERE
	`TABLE_CATALOG` = 'def' AND `TABLE_SCHEMA` = DATABASE() AND
	`TABLE_NAME` = '$wpdb->postmeta' AND `INDEX_NAME` = 'awd_datafeedr_import_performance'");

	if (is_null($indexexists)) {
		$wpdb->query("create index awd_datafeedr_import_performance on $wpdb->postmeta(meta_key, meta_value(25), post_id);"); // reduces from .5 seconds to 0 seconds the time taken to check if each product existss on peepgo with 142000 products
	}
	
	
	// Check if an update is already running.

	// Check that updates are enabled.
	$options = get_option( 'dfrps_configuration', array() );
	if ( $options['updates_enabled'] == 'disabled' ) {
		awdechotimings( "Updates disabled");
		return true;
	}

	// Check that at least 1 network is selected.
	$networks = (array) get_option( 'dfrapi_networks' );
	if ( empty( $networks['ids'] ) ) {
		awdechotimings( "No networks selected");
		return true;
	}

	// Check that at least 1 merchant is selected.
	$merchants = (array) get_option( 'dfrapi_merchants' );
	if ( empty( $merchants['ids'] ) ) {
		awdechotimings( "No merchants selected");
		return true;
	}
	
	// Return if no CPTs exist to import into.
	$registered_cpts = get_option( 'dfrps_registered_cpts', array() );
	if ( empty( $registered_cpts ) ) {
		awdechotimings( "No product sets exist");
		return true;
	} else {
		$cpts = array_keys( $registered_cpts );
		$sql_cpts = implode( "','", $cpts );
	}

	// Check that DFRPSWC importer is compatible.
	if ( defined( 'DFRPSWC_VERSION' ) ) {
		if ( version_compare( DFRPSWC_VERSION, '1.2.0', '<' ) ) {
			awdechotimings( "Product Sets incompatible - upgrade your plugin!");
			return true;
		}
	}

	global $wpdb;

	if (count($cpts) == 1) {
//	if (1==2) {
		$post = $wpdb->get_row( "		
			SELECT 
				p.*,
				update_phase.meta_value AS update_phase,
				next_update.meta_value AS next_update,
				'" . $sql_cpts[0] . "' AS cpt_type
			
			FROM $wpdb->posts p
			
			LEFT JOIN $wpdb->postmeta AS update_phase 
				ON p.ID = update_phase.post_id
				AND update_phase.meta_key = '_dfrps_cpt_update_phase'
			
			LEFT JOIN $wpdb->postmeta AS next_update 
				ON p.ID = next_update.post_id
				AND next_update.meta_key = '_dfrps_cpt_next_update_time'
			WHERE p.post_type = '" . DFRPS_CPT . "'
			AND (
				p.post_status = 'publish'
				OR 
				p.post_status = 'trash'
			)					
			ORDER BY 
				CAST(update_phase.meta_value AS UNSIGNED) DESC,
				CAST(next_update.meta_value AS UNSIGNED) ASC
				
			LIMIT 1
		", ARRAY_A );		
		awdechotimings( "Ran optimised select query");
	} else {
		
		$post = $wpdb->get_row( "		
			SELECT 
				p.*,
				update_phase.meta_value AS update_phase,
				next_update.meta_value AS next_update,
				cpt_type.meta_value AS cpt_type
			
			FROM $wpdb->posts p
			
			LEFT JOIN $wpdb->postmeta AS update_phase 
				ON p.ID = update_phase.post_id
				AND update_phase.meta_key = '_dfrps_cpt_update_phase'
			
			LEFT JOIN $wpdb->postmeta AS next_update 
				ON p.ID = next_update.post_id
				AND next_update.meta_key = '_dfrps_cpt_next_update_time'

			LEFT JOIN $wpdb->postmeta AS cpt_type 
				ON p.ID = cpt_type.post_id
				AND cpt_type.meta_key = '_dfrps_cpt_type'
				
			WHERE p.post_type = '" . DFRPS_CPT . "'
			AND (
				p.post_status = 'publish'
				OR 
				p.post_status = 'trash'
			)
			AND cpt_type.meta_value IN ('" . $sql_cpts . "')
					
			ORDER BY 
				CAST(update_phase.meta_value AS UNSIGNED) DESC,
				CAST(next_update.meta_value AS UNSIGNED) ASC
				
			LIMIT 1
		", ARRAY_A );
		awdechotimings( "Ran flexible select query");
	}
	// Return if no $post exists.
	if ( is_null( $post ) ) {
		awdechotimings( "No product sets found to update");
		return true;
	} else {
		awdechotimings( "Processing set: " . $post["ID"] . " [" . $_SERVER["REMOTE_ADDR"] . ":" . $_SERVER["SERVER_PORT"] . "] ****");
		//awdechotimings( "FILTERS: " . print_r($GLOBALS['wp_filter'],true));
	}
	// Make sure this product set is version 1.2.0 or greater.
	dfrps_upgrade_product_set_to_120( $post );

	$post = apply_filters( 'dfrps_cron_before_delete_or_update', $post );

	/**
	 * First check if post_status is 'trash'. Trashed sets
	 * get priority as we need to remove those products
	 * from the store ASAP.
	 */
 	if ( $post['post_status'] == 'trash' && $post['next_update'] <= date_i18n( 'U' ) ) {

		awdechotimings( "Running product set delete ops");
		require_once( DFRPS_PATH . 'classes/class-dfrps-delete.php' );
		new Dfrps_Delete( $post );
		return true;
	}
	
	/*
	 * If a Product Set is currently in an update phase
	 * or, if a Product Set's next update time is now
	 * or, if a Product Set's next update time is 0
	 * then, run the update.
	 */
	 
	 $localtzadjustedupdatetime = get_option( 'gmt_offset' ) * 3600 + $post['next_update'];
	if ( 
		$post['post_status'] == 'publish' &&
		(
			$post['update_phase'] > 0 ||
			$localtzadjustedupdatetime <= date_i18n( 'U' ) ||
			$post['next_update'] == 0
		) ) {
		
		if ( isset( $post['ID'] ) && ( $post['ID'] > 0 ) ) {
		
			$post['registered_cpts'] = $registered_cpts;
	
			// Update Product Set.
			require_once( DFRPS_PATH . 'classes/class-dfrps-update.php' );
			awdechotimings(" Running product set update - success!");
			new Dfrps_Update( $post );
			return true;
		}
	} else {
			awdechotimings("There was nothing to do - this product set was not scheduled to be run yet");		
			awdechotimings("Current time stamp:" . (int)date_i18n( 'U' ));
			awdechotimings("Product set update timestamp:" . (int)$post['next_update']);
	}
}

/**
 * Remove cron schedule if plugin is deactivated.
 */
register_deactivation_hook( __FILE__, 'dfrps_deactivate_cron' );
function dfrps_deactivate_cron() {
	wp_clear_scheduled_hook( 'dfrps_cron' );
}

